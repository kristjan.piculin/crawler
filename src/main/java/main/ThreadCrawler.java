package main;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import crawler.Worker;

public class ThreadCrawler extends Thread {
    private Thread t;
    private Worker spider;
    private String url;
    private int threadNo;

    public ThreadCrawler(Worker spider, int threadNo){
        this.spider = spider;
        this.threadNo = threadNo;
    }

    public void run(){
        spider.getUrl(url);
    }

    public void getUrl(String url){
        this.url = url;
    }

    public void start(){
        System.out.println("Starting thread "+ threadNo);
        if(t == null){
            t = new Thread(this, threadNo+"");
            t.start();
        }
    }


    public Worker getWorker(){
        return spider;
    }
}
