package main;

import crawler.Visited;
import crawler.Worker;
import database.Connect;
import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

public class Main {

    private static Connect data = new Connect();

    private static boolean isDomain(String link){
        return (link.split("/").length == 3);
    }

    private static boolean isFile(String link){
        return (link.contains(".pdf") ||link.contains(".doc") || link.contains(".docx")  ||
                link.contains(".ppt") || link.contains(".pptx"));
    }

    private static boolean isPicture(String link){
        return ( link.contains(".jpg") || link.contains(".gif") || link.contains(".png") );
    }

    private static boolean isGov(String link){
        return (link.contains(".gov.si"));
    }

    private static String getDomain(String url){
        String[] domainParser = url.split("/");
        return domainParser[0] + "//" + domainParser[2]+"/";
    }

    private static String downloadFile(String url){
        String[] spl = url.split("/");
        File fil = new File("downloads/"+spl[spl.length-1]);
        try {
            FileUtils.copyURLToFile(new URL(url), fil);
        }catch (MalformedURLException e){
            System.err.println("Error while downloading file" +e);
        }catch (IOException e){
            System.err.println("Error while downloading file" +e);
        }
        return fil.getName();
    }

    private static String getFileType(String link){
        if(link.contains(".pdf")){
            return ".pdf";
        }

        else if(link.contains(".jpg")){
            return ".jpg";
        }

        else if(link.contains(".gif")){
            return ".gif";
        }

        else if(link.contains(".png")){
            return ".png";
        }

        else if(link.contains(".doc")){
            return ".doc";
        }

        else if(link.contains(".docx")){
            return ".docx";
        }

        else if(link.contains(".ppt")){
            return ".ppt";
        }

        else if(link.contains(".pptx")){
            return ".pptx";
        }
        return "unknown";
    }

    private static boolean domainAlreadyExists(String domain){
        String dmn = data.getDomain(domain);
        return !(dmn.equals(""));
    }

    private static boolean containsSite(String link){
        String site = data.getPage(link);
        if(site.equals("")){
            return false;
        }
        return true;
    }

    private static int checkCollected(ThreadCrawler[] threads){
        for(int i = 0; i < threads.length; i++){
            if(threads[i].getWorker().isAllCollected()){
                return i;
            }
        }
        return 999;
    }

    private static boolean isUrl(String link){
        String[] parseUrl = link.split("/");
        return (parseUrl[0].equals("http:") || parseUrl[0].equals("https:"));
    }

    public static void main(String[] args) {
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        //Connect data = new Connect();
        data.flush(); // development - remove later
        Visited queue = new Visited();
        //Worker spider = new Worker(queue);
        ThreadCrawler[] threads = new ThreadCrawler[Integer.parseInt(args[0])];
        String[] init ={"http://evem.gov.si/", "https://e-uprava.gov.si/",  "https://podatki.gov.si/", "http://www.e-prostor.gov.si/","http://www.gov.si/", "http://www.gu.gov.si/," +
                "https://www.ess.gov.si/", "https://volitve.gov.si/", "http://www.fu.gov.si/"," www.mju.gov.si/"};
        for (String link:init) {
            data.saveSite(link, "", "");
            queue.addToQueue(link);
            data.saveUnvisitedPage(data.getDomainId(link),link);
            System.out.println(link);
        }
        System.out.println(queue.getQueueSize());
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new ThreadCrawler(new Worker(queue), i);
        }
        int active = 0;
        for (int i = 0; i < threads.length; i++) {
            String nurl = queue.pop();
            System.out.println(nurl);
            threads[i].getUrl(nurl);
            threads[i].start();
        }
        active = checkCollected(threads);
        while(queue.getQueueSize() > 0) {
            while (active == 999){
                active = checkCollected(threads);
            }
            int currentActive = active;
            String surl = threads[active].getWorker().retUrl();
            List<String> links = new ArrayList<String>();
            if (!surl.equals("") && !queue.wasVisited(surl) && !queue.isDisallowed(surl)) {
                queue.addVisitedUrl(surl);
                if(isGov(surl)) {
                    if(isDomain(surl)) {
                        String robots = threads[active].getWorker().getRobots();
                        String sitemaps = threads[active].getWorker().getSitemaps();
                        data.updateSite(surl, robots.replaceAll("\n",""), sitemaps.replaceAll("\n",""));
                    }
                    data.updatePage(surl,threads[active].getWorker().getSourceHtml(), threads[active].getWorker().getStatusCode(), threads[active].getWorker().getCurrentTime());
                    links = threads[active].getWorker().getLinks();
                    for (String lnk : links) {
                        if (!queue.wasVisited(lnk) && !lnk.equals("") && isUrl(lnk)) {
                            if (isFile(lnk)) {
                                if (!queue.isFileForbidden(lnk)) {
                                    if(!containsSite(lnk)) {
                                        data.saveUnvisitedBinary(data.getDomainId(getDomain(surl)), lnk);
                                    };
                                    queue.addFile(lnk, surl);
                                    String filname = downloadFile(lnk);
                                    String endpoint = getFileType(lnk).replace(".","").toUpperCase();
                                    int pageId = data.getPageId(lnk);
                                    data.savePageData(pageId , endpoint, "downloads/"+filname);
                                }
                            } else if (isPicture(lnk)) {
                                if (!queue.isFileForbidden(lnk)) {
                                    if(!containsSite(lnk)) {
                                        data.saveUnvisitedBinary(data.getDomainId(getDomain(surl)), lnk);
                                    }
                                    queue.addPicture(lnk, surl);
                                    downloadFile(lnk);
                                    String[] filenamel = lnk.split("/");
                                    String filename = filenamel[filenamel.length-1];
                                    Timestamp current = new Timestamp(System.currentTimeMillis());
                                    data.saveImage(data.getPageId(lnk),filename, getFileType(lnk), "downloads/"+filename, current );
                                }
                            } else {
                                if(!domainAlreadyExists(getDomain(lnk))){
                                    data.saveSite(getDomain(lnk),"","");
                                }
                                if(isDomain(lnk) && !domainAlreadyExists(getDomain(lnk))){
                                    data.saveSite(lnk,"","");
                                }
                                if(!containsSite(lnk)){
                                    data.saveUnvisitedPage(data.getDomainId(getDomain(lnk)),lnk);
                                    data.saveLink(data.getPageId(surl),data.getPageId(lnk));
                                    queue.addToQueue(lnk);
                                }
                            }
                            //System.out.println(lnk);
                        }
                    }
                    Set<String> pictures = threads[active].getWorker().getImages();
                    for (String pic : pictures) {
                        if(!containsSite(pic)) {
                            data.saveUnvisitedBinary(data.getDomainId(getDomain(surl)), pic);
                        }
                        queue.addPicture(pic, surl);
                        downloadFile(pic);
                        String[] filenamel = pic.split("/");
                        String filename = filenamel[filenamel.length-1];
                        Timestamp current = new Timestamp(System.currentTimeMillis());
                        data.saveImage(data.getPageId(pic),filename, getFileType(pic), "downloads/"+filename, current );

                    }
                }
            }
            //threads[active].getWorker().stopWorker();
            threads[active].getWorker().resetAllCollected();
            //while ( kurl.equals("") || !queue.wasVisited(kurl) || !queue.isDisallowed(kurl) || !isGov(kurl)) {
            String kurl = queue.pop();
            //}
            threads[active].getUrl(kurl);
            threads[active].getWorker().getUrl(kurl);
            active = 999;
        }
    }
}
