package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Connect {
    private Connection con;
    public Connect(){
        try {
            String url = "jdbc:postgresql://localhost:5432/crawler";
            Properties props = new Properties();
            props.setProperty("user", "kristjan");
            props.setProperty("password", "postgres");
            this.con = DriverManager.getConnection(url, props);
        }catch(java.sql.SQLException e){
            System.err.println("Error while connecting to database" + e);
        }
    }

    public void test(){
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM  crawldb.site");
            while (rs.next())
            {
                System.out.print("Column 1 returned ");
                System.out.println(rs.getString(1));
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database" + e);
        }
    }

    public void testInsert(){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.site (DOMAIN ,ROBOTS_CONTENT, SITEMAP_CONTENT) VALUES (?, ?, ?)");
            st.setString(1, "test");
            st.setString(2, "test");
            st.setString(3, "test");
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database" + e);
        }
    }

    public void saveSite(String domain, String robots, String sitemap){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.site (DOMAIN ,ROBOTS_CONTENT, SITEMAP_CONTENT) VALUES (?, ?, ?)");
            st.setString(1, domain);
            st.setString(2, robots);
            st.setString(3, sitemap);
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while inserting" + e);
        }
    }

    public int getDomainId(String domain){
        int id = 0;
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id FROM  crawldb.site where domain = '"+domain+"'");
            while (rs.next())
            {
                id = Integer.parseInt(rs.getString(1));
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("SQL error" + e);
        }

        return id;
    }

    public void savePage(int siteId, String pageTypeCode, String url, String htmlContent, int statusCode, Date accesedTime){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.page (SITE_ID ,PAGE_TYPE_CODE, URL, HTML_CONTENT," +
                    " HTTP_STATUS_CODE, ACCESED_TIME) VALUES (?, ?, ?, ?, ?, ?)");
            st.setInt(1, siteId);
            st.setString(2, pageTypeCode);
            st.setString(3, url);
            st.setString(4, htmlContent);
            st.setInt(5, statusCode);
            st.setDate(6, accesedTime);
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while inserting" + e);
        }
    }

    public void saveLink(int from, int to){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.link (FROM_PAGE, TO_PAGE) VALUES (?, ?)");
            st.setInt(1, from);
            st.setInt(2, to);
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("SQL error:" + e);
        }
    }

    public void saveImage(int pageId, String filename, String contentType, String dataPath, Timestamp accesedTime){
        try {
            File file = new File(dataPath);
            FileInputStream fis = new FileInputStream(file);
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.image (page_id, filename, content_type, data , accessed_time) " +
                    "VALUES(?,?,?,?,?)");
            st.setInt(1, pageId);
            st.setString(2, filename);
            st.setString(3, contentType);
            st.setBinaryStream(4, fis, file.length());
            st.setTimestamp(5, accesedTime);
            st.executeUpdate();
            st.close();
            fis.close();
        }catch(SQLException e){
            System.err.println("Error while inserting:" + e);
        }catch (FileNotFoundException e){
            System.err.println("File not found: "+ e);
        }catch (IOException e){
            System.err.println("IOException" + e);
        }
    }

    public void savePageData(int pageId, String dataTypeCode, String dataPath){
        try {
            File file = new File(dataPath);
            FileInputStream fis = new FileInputStream(file);
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.page_data (page_id, data_type_code, data ) " +
                    "VALUES(?,?,?)");
            st.setInt(1, pageId);
            st.setString(2, dataTypeCode);
            st.setBinaryStream(3, fis, file.length());
            st.executeUpdate();
            st.close();
            fis.close();
        }catch(SQLException e){
            System.err.println("Error while inserting pageData: " + e);
        }catch (FileNotFoundException e){
            System.err.println("File not found: "+ e);
        }catch (IOException e){
            System.err.println("IOException" + e);
        }
    }

    public String getPageCode(int code){
        String pageC = "";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT "+ code +" FROM  crawldb.page_type");
            while (rs.next())
            {
                pageC = rs.getString(1);
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database: " + e);
        }
        return pageC;
    }

    public void saveUnvisitedPage(int siteId, String url){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.page (SITE_ID ,PAGE_TYPE_CODE, URL " +
                    ") VALUES (?, ?, ?)");
            st.setInt(1, siteId);
            st.setString(2, "FRONTIER");
            st.setString(3, "'"+url+"'");
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while inserting page: " + e);
        }
    }

    public void saveUnvisitedBinary(int siteId, String url){
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO crawldb.page (SITE_ID ,PAGE_TYPE_CODE, URL " +
                    ") VALUES (?, ?, ?)");
            st.setInt(1, siteId);
            st.setString(2, "BINARY");
            st.setString(3, "'"+url+"'");
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while inserting page: " + e);
        }
    }

    public void updateSite(String site,String robots, String sitemap){
        try {
            PreparedStatement st = con.prepareStatement("UPDATE crawldb.site SET robots_content = '"+robots+"', sitemap_content = '"+sitemap+"' WHERE domain = '"+ site + "'");
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while updating site : " + e);
        }
    }

    public String getDomain(String link){
        String domain = "";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT domain FROM  crawldb.site WHERE domain = '"+link+"'");
            while (rs.next())
            {
                domain = rs.getString(1);
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database: " + e);
        }
        return domain;
    }

    public String getPage(String link){
        String page = "";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT url FROM  crawldb.page WHERE url = '"+"'"+"'"+link+"'"+"'"+"'");
            while (rs.next())
            {
                page = rs.getString(1);
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database: " + e);
        }
        return page;
    }

    public void updatePage(String url, String htmlContent, int statusCode, Timestamp accesedTime){
        try {
            PreparedStatement st = con.prepareStatement("UPDATE crawldb.page SET html_content=?, page_type_code='HTML' ,http_status_code = ?, accessed_time = ? where url = ?");
            st.setString(1, htmlContent);
            st.setInt(2,  statusCode);
            st.setTimestamp(3, accesedTime);
            st.setString(4, "'"+url+"'");
            st.executeUpdate();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while updating page: " + e);
        }
    }

    public int getPageId(String link){
        int id = 0;
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id FROM  crawldb.page WHERE url = '"+"'"+"'"+link+"'"+"'"+"'");
            while (rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            st.close();
        }catch (SQLException e){
            System.err.println("Error while connecting to the database: " + e);
        }
        return id;
    }

    public void flush(){
        try {
            PreparedStatement st0 = con.prepareStatement("DELETE FROM crawldb.link");
            PreparedStatement st1 = con.prepareStatement("DELETE FROM crawldb.image");
            PreparedStatement st2 = con.prepareStatement("DELETE FROM crawldb.page_data");
            PreparedStatement st3 = con.prepareStatement("DELETE FROM crawldb.page");
            PreparedStatement st4 = con.prepareStatement("DELETE FROM crawldb.site");
            st0.executeUpdate();
            st0.close();
            st1.executeUpdate();
            st1.close();
            st2.executeUpdate();
            st2.close();
            st3.executeUpdate();
            st3.close();
            st4.executeUpdate();
            st4.close();
        }catch (SQLException e){
            System.err.println("Error when flushing: "+ e);
        }
    }

}
