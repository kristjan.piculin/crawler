package crawler;

import java.util.*;

public class Visited {

    private Set<String> urls;
    private Set<String> hashes;
    private List<String> queue;
    private Map<String, Integer> crawlDelays;
    private Set<String> disallowed;
    private Map<String, Set<String>> pictures;
    private Map<String, Set<String>> files;
    private Map<String, Set<String>> disallowedFiles;

    public Visited(){
        this.urls = new HashSet<String>();
        this.hashes = new HashSet<String>();
        this.queue = new ArrayList<String>();
        this.crawlDelays = new HashMap<String, Integer>();
        this.disallowed = new HashSet<String>();
        this.pictures = new HashMap<String, Set<String>>();
        this.files = new HashMap<String, Set<String>>();
        this.disallowedFiles = new HashMap<String, Set<String>>();
    }

    public Boolean wasVisited(String url){
        if(this.urls.contains(url)){
            return true;
        }
        return false;
    }
    public void addVisitedUrl(String url){
        this.urls.add(url);
    }
    public Boolean checkHashes(String hash){
        if(this.hashes.contains(hash)){
            hashes.add(hash);
            return false;
        }
        return true;
    }

    public String pop(){
        String url = this.queue.get(0);
        this.queue.remove(0);
        return url;
    }

    public void addToQueue(String url){
        this.queue.add(url);
    }

    public int getQueueSize(){
        return this.queue.size();
    }

    public int getCrawlDelay(String sdomain){
        for (String link:this.crawlDelays.keySet()) {
            if(sdomain.contains(link)){
                return this.crawlDelays.get(link);
            }
        }
        return 2;
    }

    public void addCrawlDelay(String link, int time){
        this.crawlDelays.put(link, time);
    }

    public boolean isDisallowed(String link){
        for (String slink:this.disallowed) {
            if(link.contains(slink)){
                return true;
            }
        }
        return false;
    }

    public void addDisallowed(String url){
        this.disallowed.add(url);
    }

    public void addPicture(String link, String site){
        if(this.pictures.getOrDefault(site, null) == null){
            Set sitelist = new HashSet<String>();
            this.pictures.put(site,sitelist);
        }
        this.pictures.get(site).add(link);
    }

    public Set<String> getPictures(String site){
        return this.pictures.get(site);
    }

    public void addFile(String link, String site){
        if(this.files.getOrDefault(site, null) == null){
            Set sitelist = new HashSet<String>();
            this.files.put(site,sitelist);
        }
        this.files.get(site).add(link);
    }

    public Set<String> getFiles(String site){
        return this.pictures.get(site);
    }

    public void addDisallowedFile(String link, String site){
        if(this.disallowedFiles.getOrDefault(site, null) == null){
            Set sitelist = new HashSet<String>();
            this.disallowedFiles.put(site,sitelist);
        }
        this.disallowedFiles.get(site).add(link);
    }

    public Set<String> getDisallowedFiles(String site){
        return this.pictures.get(site);
    }

    public boolean isFileForbidden(String link){
        for (String lnk: this.disallowedFiles.keySet()) {
            if(link.contains(lnk)){
                for (String filname:this.disallowedFiles.get(lnk)) {
                    if(link.contains(filname)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
