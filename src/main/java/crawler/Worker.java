package crawler;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class Worker {

    private WebDriver driver;
    private String url;
    private Visited queue;
    private int crawlDelay;
    private String robots = "";
    private String sitemaps = "";
    private boolean isMainSite;
    private int statusCode;
    private Timestamp currentTime;
    private boolean allCollected = false;

    public Worker(Visited queue){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        this.driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        this.queue = queue;
        this.crawlDelay = 5;
    }

    public void getUrl(String url){
        System.out.println(url);
        isMainSite = (url.split("/").length == 3);
        this.crawlDelay = this.queue.getCrawlDelay(url);
        try {
            try {
                //TimeUnit.SECONDS.sleep(crawlDelay);
                Thread.sleep(crawlDelay*1000);
            } catch (InterruptedException e){
                System.err.println(e);
            }
            this.driver.get(url);
            this.robots="";
            this.sitemaps="";
            setCurrentTime();
            this.setStatusCode(url);
            this.url = url;
            checkRobots();
            this.allCollected = true;
        }catch (WebDriverException e){
            System.err.println("Error while accesing the site!" + e);
        }
    }

    public String getSourceHtml(){
        String source_code = "";
//        WebElement elem = driver.findElement(By.xpath("//html"));
//        String source_code = elem.getAttribute("inner_html");
        try {
            WebElement element = driver.findElement(By.xpath("//*"));
            source_code = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;", element);
        }catch (Exception e){
            System.err.println("Error getting html code: "+e);
        }
        return "<html>"+source_code+"</html>";
    }

    public List<String> getLinks(){
        List<String> links = new ArrayList<String>();
        try {
            List<WebElement> urls = driver.findElements(By.xpath("//a[@href]"));
            for (int i = 0; i < urls.size(); i++) {
                links.add(urls.get(i).getAttribute("href"));
            }
        }catch (Exception e){
            System.err.println("Error while getting links from site"+ e);
        }
        return links;
    }
    public String getSiteHash(){
        String source = this.getParsed();
        String myHash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes());
            byte[] digest = md.digest();
            myHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();
        }catch(NoSuchAlgorithmException e){
            System.err.println(e);
        }
        return myHash;
    }

    public String getSource(){
        return this.driver.getPageSource();
    }

    public Set<String> getImages(){
        Set<String> picturelist = new HashSet<String>();
        try {
            List<WebElement> links = driver.findElements(By.xpath("//img[@src]"));
            for (WebElement we: links) {
                picturelist.add(we.getAttribute("src"));
            }
        }catch (Exception e){
            System.err.println("Error when getting pictures" + e);
        }
        return picturelist;
    }

    private String getParsed(){
        return driver.findElement(By.xpath("//html")).getText();
    }

    public void stopWorker(){
        this.driver.quit();
    }

    private NodeList xmlParser(String source) {
        NodeList links = null;
        try{
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = df.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(source));
            Document doc = builder.parse(is);
            doc.getDocumentElement().normalize();
            links = doc.getElementsByTagName("url");
        }catch (Exception e){
            System.err.println(e);
        }
        return links;
    }

    private List<String> sitemapParser(String source){
        List<String> links = new ArrayList<String>();
        String[] sourcels = source.split(">");
        boolean copy = false;
        for (String srcline:sourcels) {
            if(copy){
                links.add(srcline.replace("</loc", ""));
                copy = false;
            }
            if(srcline.equals("<loc")){
                copy = true;
            }
        }
        return links;
    }

    public String getRobots(){
        return this.robots;
    }

    public String getSitemaps(){
        return this.sitemaps;
    }

    private void setStatusCode(String link){
        WebClient webClient = new WebClient();
        webClient.getOptions().setTimeout(30000);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        try{
//            String urlink = URLEncoder.encode(link,"UTF-8");
//            URL url = new java.net.URL(urlink);
            statusCode = webClient.getPage(link).getWebResponse().getStatusCode();
        }catch (IOException e){
            System.err.println("Error getting code" + e);
        }catch (Exception e){
            System.err.println("Unknown exception" + e);
        }
        webClient.close();
    }

    private int getRobotsStatusCode(String link){
        int robStatus = 0;
        WebClient webClient = new WebClient();;
        WebClientOptions wo = new WebClientOptions();
        wo.setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setTimeout(30000);
        try{
            robStatus = webClient.getPage(link).getWebResponse()
                    .getStatusCode();
        }catch (IOException e){
            System.err.println("Error getting code" + e);
        }catch (Exception e){
            System.err.println("Error getting robots status: " + e);
        }
        webClient.close();

        return robStatus;
    }

    private void checkRobots(){
        ChromeOptions options = new ChromeOptions();
        boolean start=false;
        options.addArguments("--headless");
        WebDriver robotChecker = new ChromeDriver(options);
        int status = getRobotsStatusCode(url+"robots.txt");
        boolean next = true;
        try {
            robotChecker.get(url+"robots.txt");
        }catch (WebDriverException e){
            System.out.println("There is no robot.txt file");
            next = false;
        }
        if(next && status != 404){
            String rtext = robotChecker.getPageSource();
            String[] spl = rtext.split("\n");
            String sitemap="";
            for (String line: spl) {
                this.robots+=line;
                if(line.contains("User-agent: *") || line.contains("User-Agent: *")){
                    start=true;
                }

//                else if(line.contains("User-Agent:")){
//                    start=false;
//                }
                if(start){
                    String purl="";
                    if(this.url.substring(this.url.length()-1).equals("/")){
                        purl = this.url.substring(0, this.url.length() - 1);
                    }
                    else {
                        purl = this.url;
                    }
                    if(line.toLowerCase().contains("Allow:")){
                        if(!queue.wasVisited(purl+line.split("Allow: ")[0])){
                            //queue.addToQueue(this.url+line.split("Allow: ")[0]);
                            queue.addToQueue(purl+line.replace("Allow: ",""));
                        }
                    }
                    else if(line.contains("Crawl-delay:")){
                        this.queue.addCrawlDelay(this.url, Integer.parseInt(line.replace("Crawl-delay: ", "")));
                    }

                    else if(line.contains("Disallow:")){
                        //queue.addVisitedUrl(this.url+line.split("Disallow: ")[0]);
                        queue.addDisallowed(purl+line.replace("Disallow: ", ""));
                        if(line.contains("*.pdf")){
                            queue.addDisallowedFile(".pdf", url);
                        }
                        else if(line.contains("*.jpg")){
                            queue.addDisallowedFile(".jpg", url);
                        }
                        else if(line.contains("*.jpeg")){
                            queue.addDisallowedFile(".jpeg", url);
                        }
                        else if(line.contains("*.doc")){
                            queue.addDisallowedFile(".doc", url);
                        }
                        else if(line.contains("*.docx")){
                            queue.addDisallowedFile(".docx", url);
                        }
                        else if(line.contains("*.ppt")){
                            queue.addDisallowedFile(".ppt", url);
                        }
                        else if(line.contains("*.pptx")){
                            queue.addDisallowedFile(".pptx", url);
                        }
                        else if(line.contains("*.png")){
                            queue.addDisallowedFile(".png", url);
                        }
                        else if(line.contains("*.gif")){
                            queue.addDisallowedFile(".gif", url);
                        }
                    }
                }
                if(line.contains("Sitemap:")){
                    //sitemap = line.split("Sitemap: ")[0];
                    sitemap = line.replace("Sitemap: ", "").replace("</pre></body></html>", "");
                }
            }
            if(!sitemap.equals("")){
                robotChecker.get(sitemap);
                String src = robotChecker.getPageSource();
                List<String> links = sitemapParser(src);
                for (String str:links) {
                    if(!queue.wasVisited(str) && !queue.isDisallowed(str)){
                        queue.addToQueue(str);
                        sitemaps+=str;
                    }
                }
            }
        }
        robotChecker.quit();
    }

    public int getStatusCode(){
        return this.statusCode;
    }

    public boolean isMainSite(){
        return isMainSite;
    }

    private void setCurrentTime(){
        long time =System.currentTimeMillis();
        currentTime = new java.sql.Timestamp(time);
    }

    public Timestamp getCurrentTime(){
        return this.currentTime;
    }

    public String retUrl(){
        return this.url;
    }

    public boolean isAllCollected(){
        return allCollected;
    }
    public void resetAllCollected(){
        url = "";
        robots ="";
        statusCode = 0;
        this.allCollected = false;
    }
}
