# GOV CRAWLER
GOV crawler is a crawler that crawls .gov.si sites written in java.

# SETUP
* First you have to setup postgresql database then create a new database with name crawler.
* Then there should be created user kristjan with password postgres
* In the database should be executed script from ucilnica fri
* "jdbc:postgresql://localhost:5432/crawler" 
* then in the folder crawler should be executed command "mvn clean install"
* then "mvn package"
* then "java -jar target/crawler-0.9.jar <parameter number>
* example - java -jar target/crawler-0.9.jar 5
* Tools needed maven 3.5.2 , jdk 8+
* Tested on linux for windows chromedriver should be dwonloaded and put in /crawler/src/main/resources and changed in Connect.java before building project